---
main_image: >
  /assets/img/work/CottagesAshmore-logo-dark-bg.png
large_image: >
  /assets/img/work/CottagesAshmore-sign.jpg
title: The Cottages at Ashmore
description: |
  <p>
  	    The Cottages at Ashmore is a new subdivision being developed in Red Bank, TN. I helped my friend <a href="http://www.carolineoutlaw.com">Caroline Outlaw</a> and <a href="http://www.chattanoogaremax.com">Remax Renaissance</a> design a logo and sign for the neighborhood, and worked with <a href="http://www.sceniccitysigns.com">Scenic City Signs</a> for the construction of the signage.</p>
author: jmulkey1978
live_url: ""
categories:
  - print
  - sign
  - real estate
  - logo
  - identity
gallery:
  - 
    title: Final Logo Direction
    img: >
      /assets/img/work/CottagesAshmore-logo-dark-bg.png
  - 
    title: 'The Cottages at Ashmore - Entrance Signage'
    img: >
      /assets/img/work/CottagesAshmore-sign.jpg
  - 
    title: Initial round of directions
    img: /assets/img/work/Ashmore-Cottages-1.gif
  - 
    title: ""
    img: /assets/img/work/Ashmore-Cottages-3.gif
  - 
    title: ""
    img: /assets/img/work/Ashmore-Cottages-4.gif
---
