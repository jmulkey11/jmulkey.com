---
main_image: /assets/img/work/Geneuity-thumb.jpg
title: Geneuity CRS
description: '<p>Geneuity is a clinical research lab in Maryville, TN. I was responsible for the project management and complete redesign of the website. We chose to build the website in Wordpress and we used the Bootstrap framework to address the need for a mobile-first approach. This project went from concept to completion within two months and stayed well below budget. I also provided an identity refresh for Geneuity which included an updated logo, business cards, and print design templates for flyers and stationary.</p>'
live_url: geneuity.com
gallery:
  - 
    title: Geneuity Responsive Website
    img: /assets/img/work/Geneuity-Responsive.jpg
  - 
    title: ""
    img: ""
categories: ""
---
