---
main_image: /assets/img/work/homepage.png
title: MPLNET.com
description: |
  <p>This project was a complete redesign of Molecular Pathology Laboratory Network's website. I worked with a large team of laboratory professionals, including pathologists, software developers, and the executive team. We solved several problems that we were facing with the previous site:</p><ul>
  <li>We used a responsive and mobile-first approach to develop an accessible website that works across a wide range of devices and screen sizes.</li><li>We used&nbsp;<a href="https://ellislab.com/expressionengine" target="_blank">ExpressionEngine CMS</a> ( as opposed to Joomla in the previous site ) to allow more flexibility with the front-end templating engine, provide more security, and allow more control over the complex content strategy workflows for this site.</li><li>We implemented version control with&nbsp;<a href="https://github.com/">GitHub</a>,&nbsp;<a href="http://www.git-tower.com/" target="_blank">Tower</a>, and&nbsp;<a href="http://capistranorb.com/" target="_blank">Capistrano</a>, to allow a team of developers to collaborate on the code base and have a rock-solid backup plan and development documentation.</li><li>We spent time researching our Google Analytics data in order to rethink our entire IA ( Information Architecture ) for the site, and we developed a much more intuitive navigation structure that delivers content to the user more effectively, especially in regards to the test menu.</li></ul>
live_url: mplnet.com
gallery:
  - 
    title: Homepage
    img: /assets/img/work/homepage.png
  - 
    title: Test Menu
    img: /assets/img/work/TestMenu.png
  - 
    title: About Us
    img: /assets/img/work/AboutUs.png
  - 
    title: Services
    img: /assets/img/work/Services.png
categories:
  - web
  - ExpressionEngine
  - responsive
---
