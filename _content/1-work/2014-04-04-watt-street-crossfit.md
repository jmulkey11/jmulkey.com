---
main_image: /assets/img/work/WattStreet-thumb.jpg
large_image: ""
title: Watt Street CrossFit
description: |
  <p>
  	Watt Street CrossFit is a comprehensive fitness and nutrition center based in Alcoa, TN. I helped this startup business establish a complete branding solution including the logo and identity design, signage, business cards, t-shirts, and a responsive, Wordpress-driven website.</p><p><iframe src="//player.vimeo.com/video/90212939" width="800" height="450" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="">
  	</iframe></p>
live_url: wattstreetcf.com
categories:
  - CrossFit
  - Wordpress
  - Responsive
  - Branding
  - Logo
  - Mural
  - Photography
  - Video
gallery:
  - 
    title: Responsive, Wordpress-driven Website
    img: >
      /assets/img/work/WattStreet-responsive.jpg
  - 
    title: Building Mural by Rococo Knoxville
    img: /assets/img/work/IMG-7054.jpg
  - 
    title: Business Card
    img: /assets/img/work/zpratt-bc-v1.gif
  - 
    title: T-shirt
    img: >
      /assets/img/work/Watt-St-T-Shirt-MockUp-3.jpg
---
