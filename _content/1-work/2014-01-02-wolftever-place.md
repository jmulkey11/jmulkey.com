---
main_image: /assets/img/work/wolftever-dribble.gif
large_image: ""
title: Wolftever Place
description: |
  <p>
  	Wolftever Place is a new subdivision in Ooltewah, TN. It's being marketed by Caroline Outlaw and Remax Renaissance of Chattanooga. I helped them develop an identity package which included the logo and builder signage.</p>
author: ""
live_url: ""
categories:
  - logo
  - branding
  - identity
gallery:
  - 
    title: "Builder's Sign"
    img: /assets/img/work/IMG-3423.jpg
  - 
    title: Initial logo directions
    img: /assets/img/work/Wolftever-Place-1.gif
  - 
    title: ""
    img: /assets/img/work/Wolftever-Place-2.gif
  - 
    title: ""
    img: /assets/img/work/Wolftever-Place-3.gif
---
