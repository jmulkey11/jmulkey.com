---
main_image: >
  /assets/img/work/georgia-trauma-commission.jpg
large_image: ""
title: Georgia Trauma Commission
description: |
  <p>
  	    The Georgia Trauma Care Network Commission was established in 2007 to maintain, and administer a trauma center network, to coordinate the best use of existing trauma facilities and to direct patients to the best available facility for treatment of traumatic injury and participate in the accountability mechanism for the entire Georgia trauma system, primarily overseeing the flow of funds for system improvement.</p><p>
  	    This project scope included a responsive redesign of their existing website, which was developed with ExpressionEngine and Structure. The primary challenge was that the website needed to keep all of the content and database information in place, while completely redesigning the front-end using a responsive and mobile-first approach.&nbsp;We solved a number of problems with the redesign of the website including mobile support, content strategy and a subtle brand refresh.</p>
author: jmulkey1978
live_url: georgiatraumacommission.org
categories: ""
gallery:
  - 
    title: Georgia Trauma Commission website
    img: >
      /assets/img/work/georgia-trauma-commission.jpg
  - 
    title: ""
    img: ""
  - 
    title: ""
    img: ""
  - 
    title: ""
    img: ""
---
