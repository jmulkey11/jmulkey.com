---
main_image: /assets/img/work/outtahere.jpg
title: Outta Here Waste Valet
description: |
  <p>
  	    Outta Here Waste Valet is a regional company engaged in doorstep trash and recycling pick-up for apartment complexes and collage campuses. These services provide ancillary income to the properties and a value-added amenity to the residents. I helped this startup business establish an identity with a logo design, business cards, letterhead, envelopes and a flyer. We are in the process of developing a content strategy for the website and social media campaign.
  </p>
author: jmulkey1978
live_url: outtaherewaste.com
categories:
  - branding
  - print
  - web
large_image: ""
gallery:
  - 
    title: Business Cards
    img: /assets/img/work/IMG-6965.jpg
  - 
    title: Coming Soon page
    img: >
      /assets/img/work/OuttaHere-comingsoon.png
---
