---
title: Stone Sumblin Law
categories:
  - branding
  - web
  - print
main_image: /assets/img/work/stonesumblinlaw.jpg
description: |
  <p>
  	This project included a full identity package: logo, business cards, letterhead, envelopes and a one-page website. It was created for Anne Stone Sumblin, a fourth-generation Alabama lawyer from Bay Minette, Alabama. I worked directly with Ann to develop the content and design of the website. This is a static site built with HTML, CSS, and jQuery and uses a single-page anchor navigation layout. It also uses <a href="http://cufon.shoqolate.com/generate/">cufón</a> font replacement and the elegant font <a href="http://cufon.shoqolate.com/generate/">Calluna</a> to add typographic style.
  </p>
author: jmulkey1978
live_url: stonesumblinlaw.com
large_image: ""
gallery:
  - 
    title: Home page
    img: /assets/img/work/home.png
  - 
    title: About section
    img: /assets/img/work/about.png
  - 
    title: Business cards
    img: /assets/img/work/IMG-6977.jpg
  - 
    title: Letterhead
    img: /assets/img/work/letterhead.png
---
