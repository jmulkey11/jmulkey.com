---
title: 'Indigo iOS & Web'
categories:
  - iOS
  - UI
  - branding
main_image: /assets/img/work/indigo.jpg
description: |
  <p>
  	Indigo is a web-based laboratory information portal for Molecular Pathology Laboratory Network (MPLN). It allows users to order and access patient test results from a wide range of internet-connected devices. The system currently includes a stand-alone desktop application, a web-based application, and a native iOS app for iPad and iPhone.
  </p>
  <p>
  	As the user interface designer for this project, I was responsible for creating a unique brand experience for the product that would span across the family of applications. After the identity package was complete, we started the wireframing and information architecture process, which evolved into static prototypes. I’ve been working closely with a talented group of iOS developers and .NET web service programmers to create this software. It is now in the App Store and we’re planning future updates for iOS 7.
  </p>
author: jmulkey1978
live_url: ""
large_image: ""
gallery:
  - 
    title: iPad landscape home screen
    img: >
      /assets/img/work/iPadRetina-Landing-2x.jpg
  - 
    title: iPad iOS 7 UI update
    img: /assets/img/work/iPad-iOS7.jpg
  - 
    title: ""
    img: ""
---
