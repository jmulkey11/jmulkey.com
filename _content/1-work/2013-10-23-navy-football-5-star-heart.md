---
main_image: /assets/img/work/5starheart.jpg
title: 'Navy Football - Five-Star Heart'
description: |
  <p>
  	In the military, there is no higher rank than that of five-star general or five-star admiral. Navy Football’s media department wanted something representative of the university’s military roots for use in their highlight reels. Also, the big-time, blue-chip athlete is often described as a “five-star athlete.” Navy Football believes that they win with heart, determination and teamwork and that these characteristics are more important than raw talent. So the objective of this identity project was to express the belief that five-star heart trumps five-star talent while defining who Navy Football is as a football program.
  </p>
  <p>
  	The video below was created by the Navy Football video production team and uses the 5-Star Heart logo in the opening title sequence.
  </p>
  <iframe src="//player.vimeo.com/video/55214104" width="500" height="281" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="">
  </iframe>
  <p>
  	 <a href="http://vimeo.com/55214104">2012 Navy Football Motivational vs Army</a> from <a href="http://vimeo.com/navyfootball">Navy Football Video</a> on <a href="https://vimeo.com">Vimeo</a>.
  </p>
author: jmulkey1978
live_url: vimeo.com/55214104
categories:
  - branding
  - logo
large_image: ""
gallery:
  - 
    title: '5 Star Heart - Final Direction'
    img: /assets/img/work/5StarHeart-v9c.png
  - 
    title: Horizontal Layout
    img: /assets/img/work/5SH-horiz-logo.jpg
  - 
    title: ""
    img: ""
---
