---
main_image: /assets/img/work/LedlowLaw-thumb.jpg
title: Ledlow Law P.C.
description: |
  <p>
  	 Ledlow Law P.C. is a single-person law firm that concentrates on debtor/consumer bankruptcy cases. I worked directly with Vincent Ledlow to develop this website and content strategy. He wanted a content management system that was easy to use and would allow him to manage his personal blog. We used <a href="http://ellislab.com/expressionengine">ExpressionEngine</a> and <a href="http://buildwithstructure.com/">Structure</a> to accomplish this. These tools gave us the ability to build a responsive and mobile-friendly front end for the site, while giving Vincent a user-friendly and intuitive back-end interface to manage his content.
  </p>
author: jmulkey1978
live_url: ledlowlaw.com
categories:
  - web
  - ExpressionEngine
  - content strategy
large_image: ""
gallery:
  - 
    title: Ledlow Law website
    img: /assets/img/work/LedlowLaw-large.jpg
---
