---
title: East Tennessee Rowing Organization
_fieldset: project
categories:
  - branding
  - web
  - photography
description: |
  <p>
  	     This project gave me the opportunity to provide East Tennessee Rowing Organization (ETRO) with an updated identity, website and photography.
  </p>
  <p>
  	     It started with a identity refresh because the name of the organization had changed and we wanted to use a clean and modern typeface that would reproduce well on merchandise. Using the updated logo, I also designed a T-shirt, hoodie, sweatpants and stickers for the club members.
  </p>
  <p>
  	     Since I was a member of this club for four years while I lived in Knoxville, I had quite a large collection of photos that I had taken of the venue. This gave us a nice start for the website imagery and I have added a few more recent photos from regattas and the new facilities.
  </p>
  <p>
  	     The website utilizes using ExpressionEngine CMS, HTML5, CSS3 and responsive layouts. I learned so much about these technologies and it has really changed and improved my whole web design process. This website works across a wide range of devices and browsers. ExpressionEngine allowed a great amount of flexibility with the code and didn’t limit my designs like other CMSs often do. The content editors love the control panel experience because it’s so intuitive in managing the content.
  </p>
author: jmulkey1978
main_image: /assets/img/work/igetrowing-thumb.jpg
live_url: http://igetrowing.com
large_image: ""
gallery:
  - 
    title: igetrowing.com responsive layouts
    img: /assets/img/work/igetrowing.jpg
---
