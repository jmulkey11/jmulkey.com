---
main_image: /assets/img/work/matt-burgin-bc-01.png
large_image: /assets/img/work/Burgin-responsive.jpg
title: Burgin Homebuilders
description: |
  <p>
  	    This is a complete branding project for my friend and client Matt Burgin. Matt is a general contractor in Chattanooga, TN, and the owner of&nbsp;<a href="http://burginhomebuilders.com" target="_blank">Burgin Homebuilders</a>. I worked directly with Matt to develop an identity package which included a logo, business cards, stationary, photography and video services. We recently launched a new Wordpress driven website and are in the process of planning our social media campaigns.&nbsp;</p><iframe src="//player.vimeo.com/video/80002825" width="100%" height="450" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="">
  </iframe>
author: jmulkey1978
live_url: burginhomebuilders.com
categories:
  - Logo
  - Identity
  - Web
  - Video
  - Photography
gallery:
  - 
    title: Responsive Wordpress website
    img: /assets/img/work/Burgin-responsive.jpg
  - 
    title: Final Logo Direction
    img: >
      /assets/img/work/Burgin-Home-Builders-final.png
  - 
    title: Business Cards
    img: /assets/img/work/IMG-6280.jpg
  - 
    title: ""
    img: ""
  - 
    title: Initial Directions
    img: /assets/img/work/Burgin-comps.png
  - 
    title: 'Photography & Video'
    img: /assets/img/work/IMG-5882.jpg
---
