# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Version control for jmulkey.com
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

This website is built with Statamic, a flat file CMS. It doesn’t take much to run Statamic on your server. Your server will need the following things:
* A web server such as Apache, Nginx or IIS.
* PHP 5.3.6+, compiled with the mcrypt extension
* mod_rewrite (or your server’s equivalent)
* Although not required to use Statamic, having the gd library installed will make your install even better.
* Database configuration: There is no database. As long as your server is running PHP
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Jonathan Mulkey shout@jmulkey.com | @jmulkey